package com.binhlh.testprogrammingskillsapp.model;

/**
 * Created by LHBINH on 04/10/2017.
 */

public class TopItem {
    private String tag;
    private String img;

    public TopItem() {
    }

    public TopItem(String tag, String img) {
        this.tag = tag;
        this.img = img;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
