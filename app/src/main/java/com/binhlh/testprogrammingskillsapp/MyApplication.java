package com.binhlh.testprogrammingskillsapp;

import android.app.Application;

/**
 * Created by LHBINH on 03/10/2017.
 */
public class MyApplication extends Application {

    private static MyApplication INSTANCE;

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;
    }

    public static synchronized MyApplication getInstance() {
        return INSTANCE;
    }
}
