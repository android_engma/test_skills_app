package com.binhlh.testprogrammingskillsapp.presentation;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LHBINH on 03/10/2017.
 */

public abstract class BaseAdapter<T, D extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<D> {
    protected List<T> mItems;
    protected int mLastVisibleItem, mTotalItemCount;
    protected boolean mIsLoading;

    protected OnItemRecyclerClickListener<T> mListener;
    protected OnLoadMoreListener mLoadMoreListener;


    public BaseAdapter() {
        mItems = new ArrayList<>();
    }

    public BaseAdapter(RecyclerView recyclerView) {
        mItems = new ArrayList<>();
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    mTotalItemCount = linearLayoutManager.getItemCount();
                    mLastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!mIsLoading && mLastVisibleItem == mTotalItemCount - 1) {
                        if (mLoadMoreListener != null) {
                            mLoadMoreListener.onLoadMore();
                        }
                        mIsLoading = true;
                    }
                }
            });
        }
    }

    public void setListener(OnItemRecyclerClickListener<T> listener) {
        mListener = listener;
    }

    public void setLoadMoreListener(OnLoadMoreListener listener) {
        mLoadMoreListener = listener;
    }

    public void clearAll(){
        mItems.clear();
        notifyDataSetChanged();
    }
    public void updateItems(List<T> items) {
        if (mItems != null) {
            mItems.clear();
        }
        mItems = items;
        notifyDataSetChanged();
    }

    public void updateLoadMoreItems(List<T> items) {
        if (items != null) {
            mItems.addAll(items);
            notifyDataSetChanged();
            if (items.size() < Constants.PAGE_SIZE) {
                mIsLoading = true;
            } else {
                mIsLoading = false;
            }
        }
    }


    public T getItemAtPosition(int position) {
        if (position < mItems.size()) {
            return mItems.get(position);
        }
        return null;
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }
}
