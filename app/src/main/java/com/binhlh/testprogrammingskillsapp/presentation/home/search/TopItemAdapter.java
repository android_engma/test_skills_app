package com.binhlh.testprogrammingskillsapp.presentation.home.search;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.binhlh.testprogrammingskillsapp.R;
import com.binhlh.testprogrammingskillsapp.model.TopItem;
import com.binhlh.testprogrammingskillsapp.presentation.BaseAdapter;
import com.binhlh.testprogrammingskillsapp.presentation.Utils;

/**
 * Created by LHBINH on 04/10/2017.
 */

public class TopItemAdapter extends BaseAdapter<TopItem, TopItemAdapter.ViewHolder> {

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_head, parent, false);
        return new ViewHolder(v, parent.getContext());
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        int index = (position) % mItems.size();
        TopItem topItem = mItems.get(index);
        holder.setData(topItem);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView img;
        private TextView tag;
        private Context context;

        public ViewHolder(View itemView, Context context) {
            super(itemView);
            this.context = context;
            tag = (TextView) itemView.findViewById(R.id.tv_name);
            img = (ImageView) itemView.findViewById(R.id.img_item);
        }

        void setData(TopItem topItem) {
            tag.setText(topItem.getTag());
            Utils.loadImage(this.context, img, topItem.getImg());
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size() * 10;
    }
}
