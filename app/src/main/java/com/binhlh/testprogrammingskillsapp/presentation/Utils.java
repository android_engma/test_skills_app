package com.binhlh.testprogrammingskillsapp.presentation;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;

import com.binhlh.testprogrammingskillsapp.R;
import com.bumptech.glide.Glide;

import java.io.ByteArrayOutputStream;

/**
 * Created by LHBINH on 03/10/2017.
 */

public class Utils {
    public static void loadImage(Context context, ImageView imageView, String imgUrl) {
        Glide.with(context).load(imgUrl)
                .asBitmap()
                .centerCrop()
                .error(R.mipmap.ic_launcher)
                .override(800, 600)
                .into(imageView);
    }

    public static void loadImage(Context context, ImageView imageView, Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        Glide.with(context)
                .load(stream.toByteArray())
                .asBitmap()
                .error(R.mipmap.ic_launcher)
                .into(imageView);
    }

    public static String getPhotoUrl(int farm, String server, String id, String secret) {
        String url = "https://farm" + farm + ".staticflickr.com/" + server + "/" + id + "_" + secret + ".jpg";
        return url;
    }

    public static int convertPixeltoDp(Context context, int dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        int pixels = (int) (dp * scale + 0.5f);
        return pixels;
    }
}
