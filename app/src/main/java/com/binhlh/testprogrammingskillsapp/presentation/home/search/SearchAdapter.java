package com.binhlh.testprogrammingskillsapp.presentation.home.search;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.binhlh.testprogrammingskillsapp.MyApplication;
import com.binhlh.testprogrammingskillsapp.R;
import com.binhlh.testprogrammingskillsapp.model.Photo;
import com.binhlh.testprogrammingskillsapp.presentation.BaseAdapter;
import com.binhlh.testprogrammingskillsapp.presentation.Utils;

/**
 * Created by LHBINH on 03/10/2017.
 */

public class SearchAdapter extends BaseAdapter<Photo, SearchAdapter.ViewHolder> {

    public SearchAdapter(RecyclerView recyclerView) {
        super(recyclerView);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main, parent, false);
        return new ViewHolder(view, parent.getContext());
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Photo photo = mItems.get(position);
        holder.setData(photo);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.onItemClicked(photo);
                }
            }
        });
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private Context context;
        private ImageView wideImage;
        private ImageView imgRound;
        private TextView title;
        private TextView tag;
        private LinearLayout listSmallImage;
        private TextView quote;
        private String photo_url = "";


        public ViewHolder(View itemView, Context context) {
            super(itemView);
            this.context = context;
            wideImage = (ImageView) itemView.findViewById(R.id.frame_wide_image);
            imgRound = (ImageView) itemView.findViewById(R.id.img_round);
            title = (TextView) itemView.findViewById(R.id.tv_main_title);
            quote = (TextView) itemView.findViewById(R.id.tv_quote);
            tag = (TextView) itemView.findViewById(R.id.tv_tag);
            listSmallImage = (LinearLayout) itemView.findViewById(R.id.list_small_image);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        void setData(Photo photo) {
            photo_url = Utils.getPhotoUrl(photo.getFarm(), photo.getServer(), photo.getId(), photo.getSecret());
            Utils.loadImage(context, imgRound, photo_url);
            Utils.loadImage(context, wideImage, photo_url);
            title.setText(photo.getTitle());
            quote.setText(photo.getTitle());
            tag.setText("#" + photo.getTitle());
            listSmallImage.removeAllViews();
            for (int i = 0; i < 3; i++) {
                ImageView imageView = createSmallImage(photo_url);
                listSmallImage.addView(imageView);
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) imageView.getLayoutParams();
                layoutParams.weight = 1;
                layoutParams.rightMargin = Utils.convertPixeltoDp(context, 5);
            }

        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        private ImageView createSmallImage(String url) {
            ImageView imageView = new ImageView(MyApplication.getInstance().getApplicationContext());
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.border_small_image));

            Utils.loadImage(context, imageView, url);
            return imageView;
        }
    }
}
