package com.binhlh.testprogrammingskillsapp.presentation.home.search;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.binhlh.testprogrammingskillsapp.model.SearchResponse;
import com.binhlh.testprogrammingskillsapp.model.TopItem;
import com.binhlh.testprogrammingskillsapp.presentation.Constants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LHBINH on 03/10/2017.
 */

public class SearchPresenter {
    private SearchView mView;
    private Context mContext;

    public SearchPresenter(SearchView view, Context context) {
        mView = view;
        mContext = context;
    }

    void searchPhoto(String query, int page_size, int page_count) {
        if (mView != null) {
            mView.showProgress();
            RequestQueue queue = Volley.newRequestQueue(mContext);
            String url = "https://api.flickr.com/services/rest/?method=flickr.photos.getRecent&format=json&nojsoncallback=1&api_key=1d9faeb9524cee00c0d01a98cce80779&extras=original_format&sort=" + query + "&per_page=" + page_size + "&page=" + page_count;
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new com.android.volley.Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    GsonBuilder builder = new GsonBuilder();
                    Gson gson = builder.create();
                    SearchResponse searchResponse = gson.fromJson(response, SearchResponse.class);
                    if (searchResponse != null) {
                        mView.onSuccess(searchResponse.getPhotos().getPhoto());
                        mView.hideProgress();
                    } else {
                        mView.hideProgress();
                    }
                }
            }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mView.showError("error");
                    mView.hideProgress();
                }
            });
            queue.add(stringRequest);
        }
    }

    void createTopItemList() {
        if (mView != null) {
            List<TopItem> topItems = new ArrayList<>();
            for (int i = 0; i < 21; i++) {
                topItems.add(new TopItem("#Tag: " + i, Constants.IMAGES[i]));
            }
            mView.onLoadTopItemSuccess(topItems);
        }
    }


}
