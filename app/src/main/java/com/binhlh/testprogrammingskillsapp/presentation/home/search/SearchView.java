package com.binhlh.testprogrammingskillsapp.presentation.home.search;

import com.binhlh.testprogrammingskillsapp.model.Photo;
import com.binhlh.testprogrammingskillsapp.model.TopItem;
import com.binhlh.testprogrammingskillsapp.presentation.BaseView;

import java.util.List;

/**
 * Created by LHBINH on 03/10/2017.
 */

public interface SearchView extends BaseView {
    void onSuccess(List<Photo> photos);

    void onLoadTopItemSuccess(List<TopItem> topItems);
}
