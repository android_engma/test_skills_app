package com.binhlh.testprogrammingskillsapp.presentation.home.search;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.binhlh.testprogrammingskillsapp.R;
import com.binhlh.testprogrammingskillsapp.model.Photo;
import com.binhlh.testprogrammingskillsapp.model.TopItem;
import com.binhlh.testprogrammingskillsapp.presentation.BaseFragment;
import com.binhlh.testprogrammingskillsapp.presentation.Constants;
import com.binhlh.testprogrammingskillsapp.presentation.OnLoadMoreListener;

import java.util.List;

/**
 * Created by LHBINH on 03/10/2017.
 */

public class SearchFragment extends BaseFragment implements OnLoadMoreListener, SearchView.OnQueryTextListener, com.binhlh.testprogrammingskillsapp.presentation.home.search.SearchView {
    private android.support.v7.widget.SearchView mSearchView;
    private RecyclerView mTopRecyclerView;
    private RecyclerView mBottomRecyclerView;
    private ProgressBar mProgressBar;

    private int PAGE_COUNT = 0;
    private SearchAdapter mAdapter;
    private TopItemAdapter mTopItemAdapter;
    private SearchPresenter mPresenter;
    private String query = "dogs";
    private View mRootView = null;

    @Override
    protected void mapView(View view) {
        mTopRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_top);
        mBottomRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_bottom);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        mRootView = view;
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.fragment_search;
    }

    @Override
    protected void initData() {
        setHasOptionsMenu(true);
        // init RecyclerView Top
        LinearLayoutManager linearLayoutManagerHorizontal = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false);
        mTopRecyclerView.setLayoutManager(linearLayoutManagerHorizontal);
        mTopItemAdapter = new TopItemAdapter();
        mTopRecyclerView.setAdapter(mTopItemAdapter);

        // init RecyclerView Bottom
        LinearLayoutManager linearLayoutManagerVertial = new LinearLayoutManager(getContext());
        mBottomRecyclerView.setLayoutManager(linearLayoutManagerVertial);
        mAdapter = new SearchAdapter(mBottomRecyclerView);
        mAdapter.setLoadMoreListener(this);
        mBottomRecyclerView.setAdapter(mAdapter);

        // init Presenter
        mPresenter = new SearchPresenter(this, getContext());
        mPresenter.createTopItemList();
        mPresenter.searchPhoto(query, Constants.PAGE_SIZE, PAGE_COUNT);
    }
    public int Dp2px(float dp) {
        final float scale = getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_search_fragment, menu);
        MenuItem item = menu.findItem(R.id.search_view);
        mSearchView = (android.support.v7.widget.SearchView) item.getActionView();
        EditText searchEditText = (EditText) mSearchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.colorAccent));
        searchEditText.setHintTextColor(getResources().getColor(R.color.colorAccent));
        mSearchView.setQueryHint(getString(R.string.search_query_hint));
        mSearchView.setOnQueryTextListener(this);
    }

    @Override
    protected void destroyView() {

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        PAGE_COUNT = 0;
        this.query = query;
        mAdapter.clearAll();
        mPresenter.searchPhoto(query, Constants.PAGE_SIZE, PAGE_COUNT);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Override
    public void showError(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress() {
        mRootView.setAlpha(0.8f);
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mRootView.setAlpha(1f);
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onSuccess(List<Photo> photos) {
        mAdapter.updateLoadMoreItems(photos);
    }

    @Override
    public void onLoadTopItemSuccess(List<TopItem> topItems) {
        mTopItemAdapter.updateItems(topItems);
    }

    @Override
    public void onLoadMore() {
        PAGE_COUNT++;
        mPresenter.searchPhoto(query, Constants.PAGE_SIZE, PAGE_COUNT);
    }
}
