package com.binhlh.testprogrammingskillsapp.presentation.home;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;

import com.binhlh.testprogrammingskillsapp.R;
import com.binhlh.testprogrammingskillsapp.presentation.BaseActivity;
import com.binhlh.testprogrammingskillsapp.presentation.home.friend.FriendFragment;
import com.binhlh.testprogrammingskillsapp.presentation.home.home.HomeFragment;
import com.binhlh.testprogrammingskillsapp.presentation.home.search.SearchFragment;
import com.binhlh.testprogrammingskillsapp.presentation.home.user.UserFragment;

public class HomeActivity extends BaseActivity implements ViewPager.OnPageChangeListener, TabLayout.OnTabSelectedListener {
    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private HomePagerAdapter mHomePagerAdapter;
    private Toolbar mToolbar;
    private TextView mToolbarTitleTextView;

    @Override
    protected int getResourceLayout() {
        return R.layout.activity_home;
    }

    @Override
    protected void mapView() {
        mViewPager = (ViewPager) findViewById(R.id.viewPager);
        mTabLayout = (TabLayout) findViewById(R.id.tabLayout);
        mToolbar = (Toolbar) findViewById(R.id.toolbar_main);
        mToolbarTitleTextView = (TextView) findViewById(R.id.tv_toolbar_title);
    }

    @Override
    protected void initData() {

        // setup Toolbar
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(null);

        // add fragment to viewpager
        mHomePagerAdapter = new HomePagerAdapter(getSupportFragmentManager());
        mHomePagerAdapter.addFragment(new HomeFragment());
        mHomePagerAdapter.addFragment(new SearchFragment());
        mHomePagerAdapter.addFragment(new FriendFragment());
        mHomePagerAdapter.addFragment(new UserFragment());

        // setup ViewPager Adapter
        mViewPager.setOffscreenPageLimit(4);
        mViewPager.setAdapter(mHomePagerAdapter);
        mViewPager.setCurrentItem(1);
        mViewPager.setOnPageChangeListener(this);
        mTabLayout.setupWithViewPager(mViewPager);
        mTabLayout.setOnTabSelectedListener(this);
        createTabIcon();
    }

    private void createTabIcon() {
        ImageView icon_1 = (ImageView) LayoutInflater.from(this).inflate(R.layout.custom_tablayout, null);
        icon_1.setImageResource(R.drawable.ic_action_home_page);
        mTabLayout.getTabAt(0).setCustomView(icon_1);

        ImageView icon_2 = (ImageView) LayoutInflater.from(this).inflate(R.layout.custom_tablayout, null);
        icon_2.setImageResource(R.drawable.ic_action_search);
        mTabLayout.getTabAt(1).setCustomView(icon_2);

        ImageView icon_3 = (ImageView) LayoutInflater.from(this).inflate(R.layout.custom_tablayout, null);
        icon_3.setImageResource(R.drawable.ic_action_friend);
        mTabLayout.getTabAt(2).setCustomView(icon_3);

        ImageView icon_4 = (ImageView) LayoutInflater.from(this).inflate(R.layout.custom_tablayout, null);
        icon_4.setImageResource(R.drawable.ic_action_user);
        mTabLayout.getTabAt(3).setCustomView(icon_4);
        mTabLayout.getTabAt(1).getCustomView().setAlpha(0.5f);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        mToolbarTitleTextView.setText(mHomePagerAdapter.getPageTitle(position));
    }

    @Override
    public void onPageSelected(int position) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        mViewPager.setCurrentItem(tab.getPosition());
        tab.getCustomView().setAlpha(0.5f);
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        tab.getCustomView().setAlpha(1f);

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        tab.getCustomView().setAlpha(0.5f);
    }
}
