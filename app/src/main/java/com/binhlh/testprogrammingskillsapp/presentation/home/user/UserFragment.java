package com.binhlh.testprogrammingskillsapp.presentation.home.user;

import android.view.View;

import com.binhlh.testprogrammingskillsapp.R;
import com.binhlh.testprogrammingskillsapp.presentation.BaseFragment;

/**
 * Created by LHBINH on 03/10/2017.
 */

public class UserFragment extends BaseFragment {
    @Override
    protected void mapView(View view) {

    }

    @Override
    protected int getResourceLayout() {
        return R.layout.fragment_user;
    }

    @Override
    protected void initData() {
        setHasOptionsMenu(false);
    }

    @Override
    protected void destroyView() {

    }
}
