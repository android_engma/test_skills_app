package com.binhlh.testprogrammingskillsapp.presentation;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by LHBINH on 03/10/2017.
 */

public abstract class BaseActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getResourceLayout());
        mapView();
        initData();
    }

    protected abstract int getResourceLayout();

    protected abstract void mapView();

    protected abstract void initData();

}
