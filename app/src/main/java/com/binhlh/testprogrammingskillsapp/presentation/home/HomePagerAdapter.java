package com.binhlh.testprogrammingskillsapp.presentation.home;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LHBINH on 03/10/2017.
 */

public class HomePagerAdapter extends FragmentPagerAdapter {
    private List<Fragment> mItems;

    public HomePagerAdapter(FragmentManager fm) {
        super(fm);
        mItems = new ArrayList<>();
    }

    void addFragment(Fragment fragment) {
        mItems.add(fragment);
    }

    @Override
    public Fragment getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Home";
            case 1:
                return "Search";
            case 2:
                return "Friend";
            case 3:
                return "User";
            default:
                return "Test Skills App";
        }

    }
}
