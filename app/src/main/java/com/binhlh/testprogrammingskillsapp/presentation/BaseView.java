package com.binhlh.testprogrammingskillsapp.presentation;

/**
 * Created by LHBINH on 03/10/2017.
 */

public interface BaseView {
    void showError(String message);

    void showProgress();

    void hideProgress();
}
