package com.binhlh.testprogrammingskillsapp.presentation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by LHBINH on 03/10/2017.
 */

public abstract class BaseFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(getResourceLayout(), container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapView(view);
        initData();
    }


    protected abstract void mapView(View view);

    protected abstract int getResourceLayout();

    protected abstract void initData();

    protected abstract void destroyView();


    @Override
    public void onDestroyView() {
        destroyView();
        super.onDestroyView();
    }

}
