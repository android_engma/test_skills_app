package com.binhlh.testprogrammingskillsapp.presentation;

/**
 * Created by LHBINH on 03/10/2017.
 */

public interface Constants {
    int PAGE_SIZE = 20;
    String[] IMAGES = {
            "https://farm6.staticflickr.com/5205/5333217436_ae8c784d12.jpg",
            "https://farm6.staticflickr.com/5283/5206014287_a3381edfdd.jpg",
            "https://farm8.staticflickr.com/7069/6815445970_4a5bb6328c.jpg",
            "https://farm6.staticflickr.com/5008/5227428607_6b946a83f1.jpg",
            "https://farm8.staticflickr.com/7354/8725975478_acab7bdf09.jpg",
            "https://farm9.staticflickr.com/8701/16944349157_0e56ee8649.jpg",
            "https://farm3.staticflickr.com/2574/4110026785_3b2e40b2d7.jpg",
            "https://farm6.staticflickr.com/5048/5643056758_2b52765a66.jpg",
            "https://farm4.staticflickr.com/3064/2697686505_ec683230fb.jpg",
            "https://farm9.staticflickr.com/8105/8478255627_fbf85b1552.jpg",
            "https://farm4.staticflickr.com/3178/2960116125_c6f9e7eb40.jpg",
            "https://farm8.staticflickr.com/7707/17389487171_f8b51ee48c.jpg",
            "https://farm5.staticflickr.com/4014/5123859750_1ac86128e2.jpg",
            "https://farm9.staticflickr.com/8080/8448841100_50d4fc6e4e.jpg",
            "https://farm1.staticflickr.com/775/21298229335_9bc4717885.jpg",
            "https://farm2.staticflickr.com/1181/4727440298_3f2d891f02.jpg",
            "https://farm4.staticflickr.com/3798/19037945904_98aed551ba.jpg",
            "https://farm4.staticflickr.com/3064/2697686505_ec683230fb.jpg",
            "https://farm9.staticflickr.com/8819/17055710440_352b902d47.jpg",
            "https://farm9.staticflickr.com/8439/8074350004_e373c613b7.jpg",
            "https://farm7.staticflickr.com/6087/6046193875_db2cc04d18.jpg"
    };
}
