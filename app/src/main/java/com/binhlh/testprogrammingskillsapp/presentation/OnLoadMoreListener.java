package com.binhlh.testprogrammingskillsapp.presentation;

public interface OnLoadMoreListener {
    void onLoadMore();
}
